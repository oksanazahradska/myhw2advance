const elem = document.createElement('div');
elem.id = 'root';
const ulElem = document.createElement('ul');
elem.appendChild(ulElem);

books.forEach(function(obj) {
    try {
        if (!obj.hasOwnProperty('author')) {
            throw new Error('Свойство "author" отсутствует в объекте: ' );
        }
        if (!obj.hasOwnProperty('name')) {
            throw new Error('Свойство "name" отсутствует в объекте: ' );
        }
        if (!obj.hasOwnProperty('price')) {
            throw new Error('Свойство "price" отсутствует в объекте: ' );
        }


        const liElem = document.createElement('li');
        liElem.textContent = '   Автор: ' + obj.author + '   Название: ' + obj.name + '  Цена: ' + obj.price;
        ulElem.appendChild(liElem);
    } catch (error) {
        console.error(error.message);
    }
});

document.body.appendChild(elem);
